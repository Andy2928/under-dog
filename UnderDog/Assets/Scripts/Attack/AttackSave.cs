using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSave : MonoBehaviour
{
    public GameObject m_Parent;
    float timeCount;
    [Header("��������ɶ�")]
    public float timeSet = 0.1f;
    void Update()
    {
        gameObject.transform.position = m_Parent.transform.position + Vector3.right * 0.3f + Vector3.up * 0.3f;
        timeCount = timeCount + Time.deltaTime;
        if (timeCount > timeSet || LevelSetting.deadTimeCount <= 0)
        {
            timeCount = 0;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            timeCount = 0;
            gameObject.SetActive(false);
        }
    }
}
