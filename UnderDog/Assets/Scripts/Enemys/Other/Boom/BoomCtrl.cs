using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomCtrl : MonoBehaviour
{
    Rigidbody2D rb;
    public bool beAlarmed;
    public float playerRange_x;
    public float playerRange_y;
    float playerRange = 30;
    Transform playerTarget;
    Vector3 truePlayerTarget;

    public float starkTimeCount;

    public float attackStep;

    [Header("速度")]
    public float speed = 0.5f;

    public float attackTimeCount = 0;
    [Header("下標記")]
    public float attackReadyTime = 0.1f;
    [Header("準備攻擊")]
    public float attackTime = 0.5f;
    [Header("硬直")]
    public float attackAfterTime = 2.4f;

    public GameObject attackTarget;
    public GameObject attack;

    bool attacking;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (UsageCase.isTalking == false)
        {
            truePlayerTarget = new Vector3(playerTarget.position.x, playerTarget.position.y - 0.62f, playerTarget.position.z);

            Debug.Log(playerRange_y);
            playerRange_x = Mathf.Abs(truePlayerTarget.x - gameObject.transform.position.x);
            playerRange_y = Mathf.Abs(truePlayerTarget.y - 0.62f - gameObject.transform.position.y);
            playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

            if (beAlarmed == false)
            {
                if (playerRange < 12)
                {
                    beAlarmed = true;
                }
            }
            if (beAlarmed == true)
            {
                if (starkTimeCount <= 0)
                {
                    rb.velocity = new Vector3(0, 0, 0);
                    Attack();
                    if (attackStep == 0 && playerRange > 8)
                    {
                        Move();
                    }
                }
                else
                {
                    starkTimeCount -= Time.deltaTime;
                }
            }
        }
    }

    //void OnDrawGizmos()//輔助線
    //{
    //    Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(7f, 0.5f, 0));
    //    Gizmos.color = Color.red;
    //    //Gizmos.DrawWireSphere(gameObject.transform.position, 3f);
    //}

    void Attack()
    {
        //距離
        playerRange_x = Mathf.Abs(truePlayerTarget.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(truePlayerTarget.y - 0.62f - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (playerRange < 8 && attackStep == 0)
        {
            if (playerTarget.transform.position.x > transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            if (playerTarget.transform.position.x < transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            attackStep = 1;
        }

        if (attackStep == 1)
        {
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackReadyTime)
            {
                Instantiate(attackTarget, truePlayerTarget, transform.rotation);

                attackTimeCount = 0;
                attackStep = 2;
            }
        }
        if (attackStep == 2)
        {
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackTime)
            {
                attack.SetActive(true);
                attackTimeCount = 0;
                attackStep = 3;
            }
        }
        if (attackStep == 3)
        {
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackAfterTime)
            {
                attackTimeCount = 0;
                attackStep = 0;
            }
        }
    }

    void Move()
    {
        if (playerTarget.transform.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (playerTarget.transform.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        transform.position = Vector2.MoveTowards(transform.position, playerTarget.position, speed * Time.deltaTime * 4.5f);
    }
}
