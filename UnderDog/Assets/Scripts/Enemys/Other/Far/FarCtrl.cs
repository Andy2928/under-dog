using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarCtrl : MonoBehaviour
{
    public Animator a_Far;
    Rigidbody2D rb;
    public bool beAlarmed;
    public float playerRange_x;
    public float playerRange_y;
    float playerRange = 30;
    Transform playerTarget;

    public float starkTimeCount;

    bool canAttack = false;

    public float attackStep;

    [Header("速度")]
    public float speed = 0.75f;

    public float attackTimeCount = 0;
    [Header("預警")]
    public float attackReadyTime = 0.5f;
    [Header("攻擊")]
    public float attackTime = 0.1f;
    [Header("硬直")]
    public float attackAfterTime = 1.4f;

    public GameObject attack;
    public GameObject readyAttack;

    bool attacking;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (UsageCase.isTalking == false)
        {
            playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
            playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
            playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

            if (beAlarmed == false)
            {
                if (playerRange < 9)
                {
                    beAlarmed = true;
                }
            }
            if (beAlarmed == true)
            {
                if (starkTimeCount <= 0)
                {
                    rb.velocity = new Vector3(0, 0, 0);
                    Attack();
                    if (attackStep == 0 && canAttack == false)
                    {
                        Move();
                        a_Far.SetBool("Walk", true);
                    }
                    else
                    {
                        a_Far.SetBool("Walk", false);
                    }
                }
                else
                {
                    starkTimeCount -= Time.deltaTime;
                }
            }
        }

    }

    //void OnDrawGizmos()//輔助線
    //{
    //    Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(7f, 0.5f, 0));
    //    Gizmos.color = Color.red;
    //    //Gizmos.DrawWireSphere(gameObject.transform.position, 3f);
    //}

    void Attack()
    {
        //距離
        playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (playerRange_x < 5.5f && playerRange_y < 0.15f) 
        {
            canAttack = true;
        }
        else
        {
            canAttack = false;
        }

        if (attackStep == 0 && canAttack == true)
        {
            a_Far.SetBool("Attack", true);
            if (playerTarget.transform.position.x > transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            if (playerTarget.transform.position.x < transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            attackStep = 1;
        }

        if (attackStep == 1)
        {
            readyAttack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackReadyTime)
            {
                attackTimeCount = 0;
                attackStep = 2;
            }
        }
        if (attackStep == 2)
        {
            readyAttack.SetActive(false);
            attack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackTime)
            {
                attackTimeCount = 0;
                attackStep = 3;
            }
        }
        if (attackStep == 3)
        {
            a_Far.SetBool("Attack", false);
            attack.SetActive(false);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackAfterTime)
            {
                attackTimeCount = 0;
                attackStep = 0;
            }
        }
    }

    void Move()
    {
        if (playerTarget.transform.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (playerTarget.transform.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if (playerTarget.transform.position.y - transform.position.y > .1f
            && playerTarget.transform.position.x - transform.position.x <= 5.4f
            && playerTarget.transform.position.x - transform.position.x >= -5.4f)
        {
            gameObject.transform.position += Vector3.up * Time.deltaTime * speed * 4.5f;
        }
        if (playerTarget.transform.position.y - transform.position.y < .1f
            && playerTarget.transform.position.x - transform.position.x <= 5.4f
            && playerTarget.transform.position.x - transform.position.x >= -5.4f)
        {
            gameObject.transform.position += Vector3.down * Time.deltaTime * speed * 4.5f;
        }
        if (playerTarget.transform.position.x - transform.position.x >= -5.3f)
        {
            gameObject.transform.position += Vector3.right * Time.deltaTime * speed * 4.5f;
        }
        if (playerTarget.transform.position.x - transform.position.x <= 5.3f)
        {
            gameObject.transform.position += Vector3.left * Time.deltaTime * speed * 4.5f;
        }   
    }

}
