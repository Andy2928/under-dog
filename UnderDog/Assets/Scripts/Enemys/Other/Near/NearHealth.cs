using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearHealth : MonoBehaviour
{
    public GameObject parent;
    Rigidbody2D rb;
    Transform playerTarget;

    [Header("HP")]
    public int Hp = 3;

    void Start()
    {
        rb = parent.GetComponent<Rigidbody2D>();
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    private void Update()
    {
        transform.position = parent.transform.position;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Attack")
        {
            {
                parent.GetComponent<NearCtrl>().attackStep = 0;

                parent.GetComponent<NearCtrl>().attackTimeCount = 0;
                parent.GetComponent<NearCtrl>().attack.SetActive(false);
                parent.GetComponent<NearCtrl>().readyAttack.SetActive(false);
                parent.GetComponent<NearCtrl>().a_Near.SetBool("Attack", false);

                parent.GetComponent<NearCtrl>().starkTimeCount = 0.5f;

                //補玩家血
                LevelSetting.deadTimeCount += 1;
                //扣血
                Hp -= 1;
                //死亡
                if (Hp <= 0)
                {
                    Destroy(parent);//到時候改動畫
                }

                if (playerTarget.gameObject.transform.position.x > transform.position.x)
                    rb.velocity = new Vector3(-1.541f, 0, 0);
                if (playerTarget.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(1.541f, 0, 0);
            }
        }
    }
}

