using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearCtrl : MonoBehaviour
{
    public Animator a_Near;
    Rigidbody2D rb;
    public bool beAlarmed;
    float playerRange_x;
    float playerRange_y;
    float playerRange = 30;
    Transform playerTarget;

    public float starkTimeCount;

    public float attackStep;

    [Header("速度")]
    public float speed = 0.5f;

    public float attackTimeCount = 0;
    [Header("準備時間")]
    public float attackReadyTime = 0.2f;
    [Header("亮刀時間")]
    public float attackTime = 0.1f;
    [Header("硬直時間")]
    public float attackAfterTime = 0.7f;

    public GameObject attack;
    public GameObject readyAttack;

    bool attacking;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (UsageCase.isTalking == false)
        {
            playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
            playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
            playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

            if (beAlarmed == false)
            {
                if (playerRange < 6)
                {
                    beAlarmed = true;
                }
            }
            if (beAlarmed == true)
            {
                if (starkTimeCount <= 0)
                {
                    rb.velocity = new Vector3(0, 0, 0);
                    Attack();
                    if (attackStep == 0)
                    {
                        Move();
                        a_Near.SetBool("Walk", true);
                    }
                    else
                    {
                        a_Near.SetBool("Walk", false);
                    }
                }
                else
                {
                    starkTimeCount -= Time.deltaTime;
                }
            }
        }
    }

    //void OnDrawGizmos()//輔助線
    //{
    //    Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(2f, 2, 0));
    //    Gizmos.color = Color.red;
    //    //Gizmos.DrawWireSphere(gameObject.transform.position, 3f);
    //}

    void Attack()
    {
        //距離
        playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (playerRange_x <= .9f && playerRange_y <= 0.7f && attackStep == 0)
        {
            attackStep = 1;
        }

        if(attackStep == 1)
        {
            a_Near.SetBool("Attack", true);
            readyAttack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if(attackTimeCount >= attackReadyTime)
            {
                attackTimeCount = 0;
                attackStep = 2;
            }
        }
        if (attackStep == 2)
        {
            readyAttack.SetActive(false);
            attack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackTime)
            {
                attackTimeCount = 0;
                attackStep = 3;
            }
        }
        if (attackStep == 3)
        {
            attack.SetActive(false);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackAfterTime)
            {
                a_Near.SetBool("Attack", false);
                attackTimeCount = 0;
                attackStep = 0;
            }
        }
    }

    void Move()
    {
        if (playerTarget.transform.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (playerTarget.transform.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        transform.position = Vector2.MoveTowards(transform.position, playerTarget.position, speed * Time.deltaTime * 6);
    }
}
