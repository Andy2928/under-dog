using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnimyCtrl : MonoBehaviour
{
    [Header("選擇套用模式")]
    public int enemyType;
    //開啟血量模組
    public int isHealMode;

    public GameObject enemy;
    public bool attacking = false;

    //攻擊當下主角座標
    Transform attack_Player;

    Transform m_target;
    //硬直計時
    float starkTimeCount;
    [Header("攻擊道具")] 
    public GameObject AttackTool;
    [Header("攻擊道具2")]
    public GameObject AttackTool_1;

    [Header("移動速度")]
    public float speed = 0.5f;

    //敵人數據
    int maxHp;

    //[Header("近戰小混混HP")]
    //public int closeHp = 3;
    //float nearCdCount;
    //[Header("近戰小混混CD")]
    //public float nearCdSet = 1;
    //[Header("近戰小混混硬直")]
    //public float s_NearTimeSet = 0.5f;
    //[Header("近戰小混混出刀時機")]
    //public float nearAttackTiming = 0.2f;

    /*[Header("遠程小混混HP")]
    public int farHp = 3;
    float farCdCount;
    [Header("遠程小混混CD")]
    public float farCdSet = 2;
    [Header("遠程小混混硬直")]
    public float s_FarTimeSet = 0.5f;
    [Header("遠程小混混出刀時機")]
    public float farAttackTiming = 0.4f;*/

    //[Header("炸彈小混混HP")]
    //public int boomHp = 3;
    //public float boomCdCount;
    //[Header("炸彈小混混CD")]
    //public float boomCdSet = 3;
    //[Header("炸彈小混混硬直")]
    //public float s_BoomTimeSet = 0.5f;
    //[Header("炸彈小混混出刀時機")]
    //public float boomAttackTiming = 0.1f;

    [Header("狂戰士HP")]
    public int berserkerHp = 9;
    float berserkerCdCount;
    [Header("狂戰士CD")]
    public float berserkerCdSet = 2.2f;
    [Header("狂戰士硬直")]
    public float s_BerserkerTimeSet = 2;
    [Header("狂戰士出刀時機")]
    public float berserkerAttackTiming = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        m_target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        //血量相關
        if (isHealMode == 1)
        {
            //if (enemyType == 1)
            //{
            //    maxHp = closeHp;
            //}
            //if (enemyType == 2)
            //{
            //     maxHp = farHp;
            //}
            //if (enemyType == 3)
            //{
            //    maxHp = boomHp;
            //}
            //if (enemyType == 4)
            //{
            //    maxHp = berserkerHp;
            //}
        }
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (LevelSetting.isTransitions == true && starkTimeCount <= 0 && isHealMode == 0)
    //    {
    //        if (attacking == false)
    //            Move();
    //        if (attacking == true)
    //        {
    //            //if (enemyType == 1)
    //            //{
    //            //    NearEnemy();
    //            //}
    //            if (enemyType == 2)
    //            {
    //                FarEnemy();
    //            }
    //            if (enemyType == 3)
    //            {
    //                BoomEnemy();
    //            }
    //            if (enemyType == 4)
    //            {
    //                BerserkerEnemy();
    //            }
    //        }
    //    }
    //    else
    //    {
    //        starkTimeCount -= Time.deltaTime;
    //    }
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*if (collision.gameObject.tag == "Attack" && isHealMode == 0)
        {
            ////硬直
            //if (enemyType == 1)
            //{
            //    starkTimeCount = s_NearTimeSet;
            //}
            if (enemyType == 2)
            {
                starkTimeCount = s_FarTimeSet;
            }
            if (enemyType == 3)
            {
                starkTimeCount = s_BoomTimeSet;
            }
            //擊退
            //transform.position = Vector2.MoveTowards(enemy.transform.position, collision.gameObject.transform.position, -speed);
        }
        if (collision.gameObject.tag == "Attack" && isHealMode == 1) 
        {
            //補玩家血
            LevelSetting.deadTimeCount += 1;
            //扣血
            maxHp -= 1;
            //死亡
            if (maxHp <= 0)
            {
                Destroy(enemy);//到時候改動畫
            }
        }*/
    }


    void Move()
    {
        if(m_target.transform.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (m_target.transform.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        if (enemyType == 1 || enemyType == 2 || enemyType == 3)
            transform.position = Vector2.MoveTowards(transform.position, m_target.position, speed * Time.deltaTime * 6);
        if(enemyType == 4)
            transform.position = Vector2.MoveTowards(transform.position, m_target.position, speed * Time.deltaTime * 2);
    }

    //void NearEnemy()
    //{
    //    attacking = true;
    //    nearCdCount += Time.deltaTime;

    //    if (nearCdCount >= nearAttackTiming && nearCdCount <= nearAttackTiming + 0.1f) 
    //    {
    //        AttackTool.SetActive(true);
    //    }
    //    if (nearCdCount >= nearAttackTiming + 0.1f)
    //    {
    //        AttackTool.SetActive(false);
    //    }
    //    if (nearCdCount >= nearCdSet)
    //    {
    //        nearCdCount = 0;
    //        attacking = false;
    //    }
    //}
    //void FarEnemy()
    //{
    //    attacking = true;
    //    farCdCount += Time.deltaTime;

    //    if (farCdCount >= farAttackTiming && farCdCount <= farAttackTiming + 0.01f)
    //    {
    //        if (m_target.transform.position.x < transform.position.x)
    //            Instantiate(AttackTool, transform.position, transform.rotation);
    //        if (m_target.transform.position.x > transform.position.x)
    //            Instantiate(AttackTool_1, transform.position, transform.rotation);
    //        farAttackTiming += 0.01f;
    //    }
    //    if (farCdCount >= 0.5f)
    //    {
    //        attacking = false;
    //    }
    //    if (farCdCount >= farCdSet)
    //    {
    //        farCdCount = 0;
    //    }
    //}
    //void BoomEnemy()
    //{
    //    attacking = true;
    //    boomCdCount += Time.deltaTime;

    //    if (boomCdCount >= boomAttackTiming && boomCdCount <= boomAttackTiming + 0.01f)
    //    {
    //        attack_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    //        Instantiate(AttackTool_1, attack_Player.transform.position, transform.rotation);
    //        boomAttackTiming += 0.01f;
    //        AttackTool.SetActive(true);
    //    }
    //    if(boomCdCount >= 0.5f && boomCdCount >= 0.55f)
    //    {
    //        attacking = false;
    //    }
    //    if (boomCdCount >= boomCdSet)
    //    {
    //        boomCdCount = 0;
    //    }
    //}
    void BerserkerEnemy()
    {
        attacking = true;

        AttackTool.SetActive(true);
        berserkerCdCount += Time.deltaTime;

        if (berserkerCdCount == berserkerAttackTiming)
        {
            attack_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }

        if (berserkerCdCount >= berserkerAttackTiming)
        {
            if (m_target.transform.position.x > transform.position.x)
            {
                transform.position = transform.position - Vector3.left * Time.deltaTime * speed * 5;
            }
            if (m_target.transform.position.x < transform.position.x)
            {
                transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 5;
            }
        }

        if(berserkerCdCount >= berserkerCdSet)
        {
            AttackTool.SetActive(false);
            berserkerCdCount = 0;
            attacking = false;
            starkTimeCount = s_BerserkerTimeSet;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Bunker" && attacking == true && enemyType == 4)
        {
            berserkerCdCount = berserkerCdSet;
        }
    }
}
