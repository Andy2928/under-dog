using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour
{
    public GameObject a_Boom;
    [Header("子彈沉底速度")]
    public float bulletDownSpeed;
    [Header("彈速")]
    public float bulletSpeed;
    public GameObject boom;
    public int type;//1飛彈2目的地
    Transform boomTarget;
    public GameObject boomBullet;
    public GameObject readyBoom;
    public Transform boomEnemy;
    GameObject target_boomEnemy;

    float attackCount;
    int attackStep = 0;


    private void Start()
    {
        if (type == 1)
        {
            attackStep = 0;
            transform.position = boomEnemy.transform.position;
        }
        if (type == 2)
        {
            target_boomEnemy = GameObject.FindGameObjectWithTag("BoomEnemy");
        }
    }
    void Update()
    {
        if (type == 1)
        {
            if (attackStep == 0)
            {
                boomTarget = GameObject.FindGameObjectWithTag("BoomTarget").GetComponent<Transform>();

                Vector2 direction = boomTarget.transform.position - transform.position;
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                transform.position += Vector3.down * bulletDownSpeed * Time.deltaTime;

                attackCount += Time.deltaTime;

                if(attackCount >= 0.1f)
                {
                    attackStep = 1;
                    attackCount = 0;
                }
            }
            if (attackStep == 1)
            {
                attackCount += Time.deltaTime;

                if (attackCount >= 0.8f)
                {
                    attackStep = 2;
                    attackCount = 0;
                }
            }
            if (attackStep == 2)
            {
                boomBullet.transform.position = Vector2.MoveTowards(transform.position, boomTarget.position, bulletSpeed * Time.deltaTime);
            }
        }
        if (type == 2) 
        {
            if(target_boomEnemy == null)
            {
                Destroy(gameObject);
            }
            if(target_boomEnemy != null)
            {

            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "BoomTarget"  && type == 1)
        {
            attackStep = 0;
            gameObject.transform.position = boomEnemy.transform.position + Vector3.up * 0.2f;
            boomBullet.SetActive(false);
        }
        if (collision.tag == "BoomBullet" && type == 2)
        {
            a_Boom.SetActive(true);
            readyBoom.SetActive(false);
            boom.SetActive(true);
            Destroy(gameObject, 1f);
        }
    }
}
