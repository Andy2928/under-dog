using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomHurtPlayer : MonoBehaviour
{
    float timeCount;
    private void Update()
    {
        timeCount += Time.deltaTime;
        if (timeCount >= 0.1f)
            gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PlayerFloor")
        {
            gameObject.SetActive(false);
        }
    }
}
