using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonaldHP : MonoBehaviour
{
    public int hpNum;
    public GameObject m_HP;
    // Update is called once per frame
    void Update()
    {
        if ((DonaldCtrl.hp - 1) >= hpNum) 
        {
            m_HP.SetActive(true);
        }
        else
        {
            m_HP.SetActive(false);
        }
    }
}
