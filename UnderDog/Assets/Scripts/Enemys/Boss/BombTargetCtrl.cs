using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombTargetCtrl : MonoBehaviour
{
    int isTrigger = 0;
    public int number;
    public GameObject alert;
    public GameObject ani;
    public GameObject attack;
    float aniCount;
    private void Update()
    {
        if(isTrigger == 1)
        {
            aniCount += Time.deltaTime;
            if(aniCount >= 0.5f)
            {
                alert.SetActive(false);
                ani.SetActive(true);
                attack.SetActive(true);

                Destroy(gameObject, 1);
                isTrigger = 2;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (number > 0)
        {
            if (other.tag == "DonaldBomb_" + number)
            {
                isTrigger = 1;
            }
        }
        else
        {
            if (other.tag == "DonaldBomb")
            {
                isTrigger = 1;
            }
        }
    }
}
