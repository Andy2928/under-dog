using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DonaldCtrl : MonoBehaviour
{
    [SerializeField] float maxX;
    [SerializeField] float maxY;
    [SerializeField] float minX;
    [SerializeField] float minY;
    public float InvincibleTime = 0.1f;
    public float InvincibleTimeCount;

    public Animator a_Donald;
    [Header("���D�y��")]
    public GameObject jumpTarget;
    
    Transform playerTarget;//�p��Z��
    Transform attackTarget;
    public float playerRange_x;
    public float playerRange_y;
    public float playerRange;

    static public float hpPhase = 1;//�ĴX���q
    [Header("�C���q��q")]
    static public float hp = 20;//�@���q20��

    [Header("�^��t��")]
    public float hpAddTimeSet = 1;
    public float hpAddCount = 0;//�ɦ�p��
                         
    [Header("�i�^��ɶ�")]
    public float hpCanAddTimeSet = 2;
    public float hpCanAddCount = 0;//�Q���ɦ夤�_

    //�P�_�����ɥ��k
    bool isLeft;
    bool isRight;

    //0�S�����A1���Y�L�A2���D�����A3��h�u�å�
    public float attackType = 0;
    [Header("�W�O�@��")]
    public GameObject m_Attack;
    //���Y�L�N�o
    public float attackColdSet = 5;
    public float attackColdCount;
    public float attackCount = 0;
    float s_Miss = 1;


    [Header("�t��")]
    public float speed = 0.5f;

    [Header("���D����")]
    public GameObject m_Attack_1;
    [SerializeField] GameObject jumpfloor;
    float jumpRange_x, jumpRange_y, jumpRange;

    [Header("��h�u�å�")]
    public int attackAmount = 6;
    public GameObject[] attack_3;
    public GameObject[] attackTarget_3;
    float attack3ColdCount;


    void Start()
    {
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        hpPhase = 1;
        hp = 20;
    }

    void Update()
    {
        if (InvincibleTimeCount >= 0)
            InvincibleTimeCount -= Time.deltaTime;

        if (UsageCase.isTalking == false)
        {
            PlayerCtrl.donaldTarget = this.gameObject.transform;
            
            HpStage();
            if(hp > 0)
            {
                HpAdd();
                Attack();
                Move();
            }
        }
    }



    void HpStage()//�������q
    {
        if (hp <= 0 && hpPhase == 1)
        {
            gameObject.tag = "Untagged";
            a_Donald.SetBool("Dead", true);
            a_Donald.SetBool("Attack", false);
            a_Donald.SetBool("Attack_1", false);
            a_Donald.SetBool("Attack_2", false);
            a_Donald.SetBool("Walk", false);
        }

        if (LevelSetting.b_1 == true)
        {
            hp = 0;
        }
        /*if (hp <= 0 && hpPhase == 2)
        {
            LevelSetting.deadTimeCount += 20;
            Destroy(gameObject);
        }*/
    }

    void HpAdd()//�ɦ�
    {
        if (hp < 20 && hp > 0) 
        {
            if (hpCanAddCount <= hpCanAddTimeSet)
            {
                hpCanAddCount += Time.deltaTime;
            }
            else
            {
                hpAddCount += Time.deltaTime;
                if (hpAddCount >= hpAddTimeSet)
                {
                    hp += 1;
                    hpAddCount = 0;
                }
            }
        }
    }

    void Move()
    {
        if (attackType == 0)
        {
            a_Donald.SetBool("Walk", true);
            if (attackColdCount > 0)
            {

                if (playerTarget.transform.position.x > transform.position.x && transform.position.x > minX)
                {
                    transform.eulerAngles = new Vector3(0, 180, 0);
                    transform.position += Vector3.left * speed * Time.deltaTime * 1.732f;
                }
                if (playerTarget.transform.position.x < transform.position.x && transform.position.x < maxX)
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    transform.position += Vector3.right * speed * Time.deltaTime * 1.732f;
                }
            }
            else
            {
                if (playerTarget.transform.position.x > transform.position.x && transform.position.x < maxX)
                {
                    transform.eulerAngles = new Vector3(0, 180, 0);

                    transform.position += Vector3.right * speed * Time.deltaTime * 1.732f;
                }
                if (playerTarget.transform.position.x < transform.position.x && transform.position.x > minX)
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    transform.position += Vector3.left * speed * Time.deltaTime * 1.732f;
                }
            }
            if (playerTarget.transform.position.y > transform.position.y && transform.position.y < maxY)
            {
                transform.position += Vector3.up * speed * Time.deltaTime * 1.732f;
            }
            if (playerTarget.transform.position.y < transform.position.y && transform.position.y > minY)
            {
                transform.position += Vector3.down * speed * Time.deltaTime * 1.732f;
            }
        }
    }

    void Attack()
    {
        if(attackColdCount >= 0)
        {
            attackColdCount -= Time.deltaTime;
        }
        if(attack3ColdCount >= 0)
        {
            attack3ColdCount -= Time.deltaTime;
        }

        //�Z��
        playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if(attackType == 0)
        {
            m_Attack.SetActive(false);
            m_Attack_1.SetActive(false);

            if (playerTarget.transform.position.x > transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            if (playerTarget.transform.position.x < transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
        }
        //�P�_��������
        //0�S�����A1�W�O�@���A2���D�����A3��h�u�å�

        if (playerRange_x <= 4 && (transform.position.y - playerTarget.position.y) <= 0.2f && (transform.position.y - playerTarget.position.y) >= -0.4f && attackType == 0 && attackColdCount <= 0) 
        {
            attackType = 1;
        }
        if (playerRange >= 6 && attackType == 0) 
        {
            attackType = 2;
        }
        if (playerRange <= 4 && attackType == 0 && attackColdCount > 0 && attack3ColdCount <= 0 && attackColdCount > 0) 
        {
            attackType = 3;
        }
        //�W�O�@��
        if (attackType == 1)//�w��
        {
            a_Donald.SetBool("Attack", true);
            if (playerTarget.position.x - gameObject.transform.position.x > 0)
            {
                isRight = true;
            }
            if (playerTarget.position.x - gameObject.transform.position.x <= 0)
            {
                isLeft = true;
            }
            attackCount = 0;
            attackType = 1.1f;
        }
        else if (attackType == 1.1f)//�wĵ
        {
            attackCount += Time.deltaTime;

            if (attackCount >= 1f)
            {
                m_Attack.SetActive(true);

                attackCount = 0;
                attackType = 1.2f;
            }
        }
        else if (attackType == 1.2f)//�Į�
        {
            attackCount += Time.deltaTime;

            if (isRight == true)
            {
                transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 32;
            }
            if (isLeft == true)
            {
                transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 32;
            }
            if (attackCount >= 0.25f)
            {
                m_Attack.SetActive(false);
                isRight = false;
                isLeft = false;
                attackType = 1.3f;
                attackCount = 0;
                attackColdCount = attackColdSet;
            }
        }
        else if (attackType == 1.3f)//�Į�
        {
            attackCount += Time.deltaTime;

            if (attackCount >= 1)
            {
                a_Donald.SetBool("Attack", false);
                attackCount = 0;
                attackType = 0;
            }
        }
        //���D����
        if (attackType == 2)
        {
            a_Donald.SetBool("Attack_1", true); 
            
            attackCount += Time.deltaTime;
            if (attackCount > 0.75f)
            {
                attackType = 2.1f;
                attackCount = 0;
            }
        }
        if(attackType == 2.1f)
        {
            Instantiate(jumpTarget, playerTarget.transform.position, transform.rotation);
            attackType = 2.2f;
        }
        if (attackType == 2.2f)//���D
        {
            if (attackTarget == null)
            {
                attackTarget = GameObject.FindGameObjectWithTag("JumpTarget").GetComponent<Transform>();
                jumpRange_x = Mathf.Abs(attackTarget.position.x - gameObject.transform.position.x);
                jumpRange_y = Mathf.Abs(attackTarget.position.y - gameObject.transform.position.y);
                jumpRange = Mathf.Sqrt(jumpRange_x * jumpRange_x + jumpRange_y + jumpRange_y);
            }
            if (attackTarget != null)
            {
                transform.position = Vector2.MoveTowards(transform.position, attackTarget.position, speed * jumpRange * 2.3f * Time.deltaTime);
            }
            attackCount += Time.deltaTime;
            if (attackCount > 1)
            {
                Instantiate(jumpfloor, transform.position + Vector3.up * 2.2f + Vector3.left * 0.1f, transform.rotation) ;
                attackType = 2.3f;
                attackCount = 0;
            }
        }
        if (attackType == 2.3f)//����
        {
            attackCount += Time.deltaTime;
            m_Attack_1.SetActive(true);
            if (attackCount > 0.5f)
            {
                attackType = 2.4f;
                attackCount = 0;
            }
        }
        if (attackType == 2.4f)//�w��
        {
            attackCount += Time.deltaTime;
            m_Attack_1.SetActive(false);
            if (attackCount > 1.9f)
            {
                a_Donald.SetBool("Attack_1", false);
                attackType = 0;
                attackCount = 0;
            }
        }

        if (attackType == 3)
        {
            a_Donald.SetBool("Attack_2", true);

            attackType = 3.1f;
        }
        if (attackType == 3.1f)
        {
            for (int i = 0; i < attackAmount - 1; i++)
            {
                float x = Random.Range(-2f, 2f);
                float y = Random.Range(-2f, 2f);
                for (int r = 4; (x * x + y * y) > r;)
                {
                    if (x > 0)
                        x -= 0.1f;
                    else
                        x += 0.1f;

                    if (y > 0)
                        y -= 0.1f;
                    else
                        y += 0.1f;
                }
                Instantiate(attackTarget_3[i], gameObject.transform.position + new Vector3(x, y - 0.9f, 0), transform.rotation);
            }
            attackType = 3.2f;
        }
        if(attackType == 3.2f)
        {
            attackCount += Time.deltaTime;
            if (attackCount >= 0.25f)
            {
                for (int i = 0; i < attackAmount - 1; i++)
                {
                    Instantiate(attack_3[i], transform.position + Vector3.up * -0.9f, transform.rotation);
                }
                attackType = 3.3f;
                attackCount = 0;
            }
        }
        if (attackType == 3.3f)
        {
            attackCount += Time.deltaTime;
            if (attackCount >= 0.5f)
            {
                a_Donald.SetBool("Attack_2", false);
                attackType = 0;
                attackCount = 0;
                attack3ColdCount = 2;
            }
        }
    }
}
