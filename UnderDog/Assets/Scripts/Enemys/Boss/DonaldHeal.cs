using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonaldHeal : MonoBehaviour
{
    public GameObject parent;
    Transform playerTarget;

    void Start()
    {
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    private void Update()
    {
        transform.position = parent.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && parent.GetComponent<DonaldCtrl>().attackType == 1.2f)
        {
            parent.GetComponent<DonaldCtrl>().attackType = 0;
            parent.GetComponent<DonaldCtrl>().attackCount = 0;
            parent.GetComponent<DonaldCtrl>().a_Donald.SetBool("Attack", false);
            parent.GetComponent<DonaldCtrl>().attackColdCount = parent.GetComponent<DonaldCtrl>().attackColdSet;
        }

        if (collision.gameObject.tag == "Attack" && parent.GetComponent<DonaldCtrl>().attackType != 2 && parent.GetComponent<DonaldCtrl>().attackType != 2.1f && parent.GetComponent<DonaldCtrl>().attackType != 2.2f && parent.GetComponent<DonaldCtrl>().InvincibleTimeCount <= 0)//�Q����
        {
            parent.GetComponent<DonaldCtrl>().InvincibleTimeCount = parent.GetComponent<DonaldCtrl>().InvincibleTime;

            DonaldCtrl.hp -= 1;
            parent.GetComponent<DonaldCtrl>().hpCanAddCount = 0;
            parent.GetComponent<DonaldCtrl>().hpAddCount = 0;
            if (playerTarget.position.x > transform.position.x && transform.eulerAngles.y == 0)
            {
                DonaldCtrl.hp -= 1;
            }
            if (playerTarget.position.x < transform.position.x && transform.eulerAngles.y == 180)
            {
                DonaldCtrl.hp -= 1;
            }
        }
    }
}
