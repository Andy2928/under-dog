using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCtrl : MonoBehaviour
{
    public int num;
    GameObject target;
    private void Start()
    {
        if (num > 0)
        {
            target = GameObject.FindGameObjectWithTag("DonaldBombTarget_" + num);
        }
        else
        {
            target = GameObject.FindGameObjectWithTag("DonaldBombTarget");
        }
    }
    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target.transform.position, 6 * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (num > 0)
        {
            if (collision.tag == "DonaldBombTarget_" + num)
            {
                Destroy(gameObject, 0.5f);
            }
        }
        else
        {
            if (collision.tag == "DonaldBombTarget")
            {
                Destroy(gameObject, 0.5f);
            }
        }
    }
}
