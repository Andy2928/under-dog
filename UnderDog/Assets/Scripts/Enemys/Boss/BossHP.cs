using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHP : MonoBehaviour
{
    [SerializeField]
    int type;
    // Update is called once per frame
    void Update()
    {
        if (type == 0)
        {
            if (DonaldCtrl.hp <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
