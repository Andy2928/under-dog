using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCtrl : MonoBehaviour
{
    public GameObject CanTalk;
    public GameObject CantTalk;
    public int Number = 0;
    /*
    0:�˦b�s�]���f��NPC


    */
    void Update()
    {
        if(Number == 0)
        {
            if (LevelSetting.talkWithNpc == false)
            {
                gameObject.SetActive(true);
            }
            if (LevelSetting.talkWithNpc == true)
            {
                CanTalk.SetActive(false);
                CantTalk.SetActive(true);
            }
            if(LevelSetting.flag_8 == true)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
