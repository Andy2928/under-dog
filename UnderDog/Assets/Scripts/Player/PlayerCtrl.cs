using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCtrl : MonoBehaviour
{
    [SerializeField] 
    GameObject mus_Fight_Street;
    [SerializeField]
    GameObject mus_Fight_Boss;

    [SerializeField]
    GameObject mus_Livingroom;
    [SerializeField]
    GameObject mus_Bar;
    [SerializeField]
    GameObject mus_Street;

    GameObject enemySave;//偵測有沒有敵人
    [Header("主角動畫控制器")]
    public Animator a_Player;
    [Header("戰鬥主角動畫控制器")]
    public Animator a_FightPlayer;
    Rigidbody2D rb;

    [Header("主角控制器")]
    public GameObject playerController;
    //[Header("麻痺")]
    //public GameObject m_Paralysis;
    //[Header("中毒")]
    //public GameObject m_Poisoning;

    public static Transform donaldTarget;//定位唐納德

    //攻擊
    float attackTimeCount = 0;
    float attackLinkTimeCount = 0;
    [Header("攻擊冷卻時間")]
    public float attackTimeSet = 0.33f;
    [Header("攻擊銜接時間")]
    public float attackLinkTimeSet = 0.2f;
    [Header("普通攻擊第一段")]
    public GameObject m_attack;
    [Header("普通攻擊第二段")]
    public GameObject m_attack_1;
    public bool b_IsAttack = false;
    bool b_AttackLink_1 = false;
    //移動
    [Header("移動速度")]
    public float speed;
    public static bool cantUp;
    public static bool cantDown;
    public static bool cantLeft;
    public static bool cantRight;
  

    [Header("跑步點擊間隔")]
    bool readyRun = false;

    //硬直
    public static float starkTimeCount = 0;
    [Header("攻擊結束硬直時間")]
    public float s_AttackTimeSet = 0.1f;
    [Header("被攻擊硬直時間")]
    public float s_AttackedTimeSet = 0.5f;

    //互動
    public GameObject m_InteractiveButtom;
    public GameObject m_Player;

    //無敵幀
    public static float invincibleTimeCount;
    [Header("衝刺無敵時間")]
    public float rushInvincibleTimeSet = 0.25f;
    [Header("被攻擊無敵時間")]
    public float attackedInvincibleTimeSet = 0.5f;
    //float scriptAttackedAnimationTime = 0; //跳動打擊感   

    [Header("(要和唐納德<抱頭摔衝抱時間>一樣)")]
    public float hugHeadInAnimation = 1;

    //和NPC對話
    //酒吧門口倒地的NPC
    bool b_NPC;

    //場景傳送點
    bool b_FinishPoint;//往下一關
    bool b_BackPoint;//往上一關
    bool b_BarDoor;
    bool b_ElevatorDoor;


    // Start is called before the first frame update
    void Start()
    {
        LevelSetting.m_Player = this.gameObject;

        attackTimeCount = 0;
        b_IsAttack = false;
        m_InteractiveButtom.SetActive(false);

        rb = playerController.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        enemySave = GameObject.FindGameObjectWithTag("Enemy");
        m_InteractiveButtom.transform.position = gameObject.transform.position + Vector3.up;//互動提示定位
        ChangeMusic();

        if (UsageCase.isTalking == false)
        {
            CheatBtn();
            PlayerInteractive();
            //無敵幀
            if (invincibleTimeCount >= 0)
            {
                invincibleTimeCount -= Time.deltaTime;
            }

            //硬直
            if (starkTimeCount <= 0 && PlayerHealth.paralysTime <= 0)
            {
                a_FightPlayer.SetBool("Flash", false);
                a_FightPlayer.SetBool("Hurt", false);
                rb.velocity = new Vector3(0, 0, 0);
                if (LevelSetting.isTransitions == true && LevelSetting.b_PlayerIsDead == false)
                {
                    Attack();
                    if (attackLinkTimeCount <= 0)
                    {
                        PlayerMove();
                    }
                }
            }
            if (starkTimeCount >= 0)
            {
                starkTimeCount -= Time.deltaTime;
            }
        }
    }

    void ChangeMusic()
    {
        if (enemySave == null)
        {
            if (LevelSetting.nowLevel == 0)//客廳
            {
                mus_Livingroom.SetActive(true);

                mus_Bar.SetActive(false);
                mus_Street.SetActive(false);
                mus_Fight_Boss.SetActive(false);
                mus_Fight_Street.SetActive(false);
            }
            else if (LevelSetting.nowLevel == 1)//酒吧
            {
                mus_Bar.SetActive(true);

                mus_Livingroom.SetActive(false);
                mus_Street.SetActive(false);
                mus_Fight_Boss.SetActive(false);
                mus_Fight_Street.SetActive(false);
            }
            else if (LevelSetting.nowLevel == 8)//地牢
            {
                mus_Bar.SetActive(false);
                mus_Livingroom.SetActive(false);
                mus_Street.SetActive(false);
                mus_Fight_Boss.SetActive(false);
                mus_Fight_Street.SetActive(false);
            }
            else//在街上野
            {
                mus_Street.SetActive(true);

                mus_Livingroom.SetActive(false);
                mus_Bar.SetActive(false);
                mus_Fight_Boss.SetActive(false);
                mus_Fight_Street.SetActive(false);
            }
        }
        if (enemySave != null)
        {
            if (LevelSetting.nowLevel == 9 && LevelSetting.b_1 == false)//Boss戰
            {
                mus_Fight_Boss.SetActive(true);

                mus_Livingroom.SetActive(false);
                mus_Bar.SetActive(false);
                mus_Street.SetActive(false);
                mus_Fight_Street.SetActive(false);
            }
            else//在街上野
            {
                mus_Fight_Street.SetActive(true);

                mus_Livingroom.SetActive(false);
                mus_Bar.SetActive(false);
                mus_Street.SetActive(false);
                mus_Fight_Boss.SetActive(false);
            }
        }
    }
    //攻擊
    public void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && enemySave != null)// && b_IsAttack == false
        {
            if (playerController.transform.eulerAngles.y == 180)
            {
                rb.velocity = new Vector3(-2.55f, 0, 0);
            }
            if (playerController.transform.eulerAngles.y == 0)
            {
                rb.velocity = new Vector3(2.55f, 0, 0);
            }

            if (b_AttackLink_1 == true)//二段攻擊
            {
                a_FightPlayer.SetBool("Attack_1", true);
                a_FightPlayer.SetBool("Attack", false);
                b_AttackLink_1 = false;
                starkTimeCount = s_AttackTimeSet;
                attackLinkTimeCount = attackLinkTimeSet;
                m_attack_1.SetActive(true);
            }
            else if (b_AttackLink_1 == false)//一段攻擊
            {
                a_FightPlayer.SetBool("Attack", true);
                a_FightPlayer.SetBool("Attack_1", false);
                b_AttackLink_1 = true;
                starkTimeCount = s_AttackTimeSet;
                attackLinkTimeCount = attackLinkTimeSet;

                m_attack.SetActive(true);
            }

            b_IsAttack = true;
        }

        if (b_IsAttack == true)
        {
            attackTimeCount = attackTimeCount + Time.deltaTime;
            if (attackTimeCount > attackTimeSet)
            {
                b_IsAttack = false;
                attackTimeCount = 0;
            }
        }

        if(attackLinkTimeCount > 0)
        {
            attackLinkTimeCount -= Time.deltaTime;
        }
        if(attackLinkTimeCount <= 0)
        {
            b_AttackLink_1 = false;
            a_FightPlayer.SetBool("Attack", false);
            a_FightPlayer.SetBool("Attack_1", false);
        }
    }
    //移動
    public void PlayerMove()
    {
        //走與跑
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            readyRun = true;
        }
        if (!Input.GetKey(KeyCode.LeftShift))
        {
            a_Player.SetBool("Run", false);
            a_FightPlayer.SetBool("Run", false);
            readyRun = false;
        }
        

        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D) && cantUp == false && cantRight == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 4.24f;
            }
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D) && cantDown == false && cantRight == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 4.24f;
            }
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A) && cantDown == false && cantLeft == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 4.24f;
            }
        }
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A) && cantUp == false && cantLeft == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 4.24f;
            }
        }
        if (!(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A)) && 
            !(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A)) &&
            !(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D)) &&
            !(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)))
        {
            if (Input.GetKey(KeyCode.A) && cantLeft == false)
            {
                if (readyRun == false)
                {
                    playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 3;
                    //readyRun_L = true;
                }
                else
                {
                    playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 6;
                }
            }
            else if (Input.GetKey(KeyCode.D) && cantRight == false)
            {
                if (readyRun == false)
                {
                    playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position - Vector3.left * Time.deltaTime * speed * 3;
                    //readyRun_R = true;
                }
                if (readyRun == true)
                {
                    playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position - Vector3.left * Time.deltaTime * speed * 6;
                }
            }
            else if (Input.GetKey(KeyCode.W) && cantUp == false)
            {
                if (readyRun == false)
                {
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 3;
                    //readyRun_U = true;
                }
                if (readyRun == true)
                {
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 6;
                }
            }
            else if (Input.GetKey(KeyCode.S) && cantDown == false)
            {
                if (readyRun == false)
                {
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position - Vector3.up * Time.deltaTime * speed * 3;
                    //readyRun_D = true;
                }
                if (readyRun == true)
                {
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position - Vector3.up * Time.deltaTime * speed * 6;
                }
            }
            else
            {
                a_Player.SetBool("Walk", false);
                a_FightPlayer.SetBool("Walk", false);
                //    if (isRun_U == true || isRun_D == true || isRun_L == true || isRun_R == true)
                //    {
                //        readyRunTimeCount_U = readyRunTimeSet;
                //        readyRunTimeCount_D = readyRunTimeSet;
                //        readyRunTimeCount_L = readyRunTimeSet;
                //        readyRunTimeCount_R = readyRunTimeSet;
                //        isRun_U = false;
                //        isRun_D = false;
                //        isRun_L = false;
                //        isRun_R = false;
                //    }
            }
        }
        //if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        

        //衝刺
        if (Input.GetKeyDown(KeyCode.Space) && enemySave != null)// && isRush == false
        {
            starkTimeCount = 0.25f;
            if (Input.GetKey(KeyCode.W) && cantUp == false)
            {
                a_FightPlayer.SetBool("Flash", true);
                rb.velocity = new Vector3(0, 9.37f, 0);
                invincibleTimeCount = rushInvincibleTimeSet;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
            }
            else if (Input.GetKey(KeyCode.S) && cantDown == false)
            {
                a_FightPlayer.SetBool("Flash", true);
                rb.velocity = new Vector3(0, -9.37f, 0);
                invincibleTimeCount = rushInvincibleTimeSet;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
            }
            else if (Input.GetKey(KeyCode.A) && cantLeft == false)// || gameObject.transform.eulerAngles.y == 180 && cantRight == false
            {
                a_FightPlayer.SetBool("Flash", true);
                rb.velocity = new Vector3(-9.37f, 0, 0);
                invincibleTimeCount = rushInvincibleTimeSet;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
            }
            else if (Input.GetKey(KeyCode.D) && cantRight == false)// || gameObject.transform.eulerAngles.y == 0 && cantRight == false
            {
                a_FightPlayer.SetBool("Flash", true);
                rb.velocity = new Vector3(9.37f, 0,0);
                invincibleTimeCount = rushInvincibleTimeSet;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
            }
        }
    }
    void CheatBtn()
    {
        if(Input.GetKeyDown(KeyCode.O))
        {
            m_Player.transform.position = new Vector3(-5, -1.5f, 0);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            m_Player.transform.position = new Vector3(-5, -0.5f, 0);
        }

    }
    //交互
    void PlayerInteractive()///////////////////////////////////////////////切換關卡
    {
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_InteractiveButtom.SetActive(false);
            cantDown = false;
            cantLeft = false;
            cantRight = false;
            cantUp = false;
            if (b_FinishPoint == true)
            {
                m_Player.transform.position = new Vector3(-5, -1.5f, 0);
                b_FinishPoint = false;
                if (LevelSetting.nowLevel == -2 && LevelSetting.flag_10 == true)//1-1-2 -> 1-2-2
                {
                    LevelSetting.nowLevel = -1;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(4);
                }
                else if (LevelSetting.nowLevel == -4)//1-1 -> 1-2
                {
                    LevelSetting.nowLevel = -3;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(2);
                }
                else if (LevelSetting.nowLevel == 4)//1-5 -> 1-4
                {
                    LevelSetting.nowLevel = 3;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(8);
                }
                else if (LevelSetting.nowLevel == 8)//1-9 -> 1-10
                {
                    m_Player.transform.position = new Vector3(0, -0.5f, 0);
                    LevelSetting.nowLevel = 9;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(14);
                }
                else if (LevelSetting.nowLevel == 5)//1-6 -> 1-7
                {
                    LevelSetting.nowLevel = 6;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(11);
                    m_Player.transform.position = new Vector3(-5, -1.9f, 0);
                }
                else if (LevelSetting.nowLevel == 6)//1-7 -> 1-8
                {
                    LevelSetting.nowLevel = 7;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(12);
                }
            }
            if(b_BackPoint == true)
            {
                b_BackPoint = false;
                //根據關卡傳送

                m_Player.transform.position = new Vector3(21, -1.5f, 0);
                if (LevelSetting.nowLevel == -2 && LevelSetting.flag_10 == true)//1-1-2 -> 1-3
                {
                    LevelSetting.nowLevel = 2;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(7);
                }
                else if (LevelSetting.nowLevel == -1)//1-2-2 -> 1-1-2
                {
                    LevelSetting.nowLevel = -2;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(3);
                }
                else if (LevelSetting.nowLevel == 3)//1-4 -> 1-5
                {
                    LevelSetting.nowLevel = 4;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(9);
                }
                else if (LevelSetting.nowLevel == 4)//1-5 -> 1-6
                {
                    m_Player.transform.position = new Vector3(-5, -1.5f, 0);
                    LevelSetting.nowLevel = 5;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(10);
                }
                else if (LevelSetting.nowLevel == 6)//1-7 -> 1-6
                {
                    LevelSetting.nowLevel = 5;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(10);
                }
                else if (LevelSetting.nowLevel == 7)//1-8 -> 1-7
                {
                    LevelSetting.nowLevel = 6;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(11);
                    m_Player.transform.position = new Vector3(21, -1.9f, 0);
                }
                else if (LevelSetting.nowLevel == 5)//1-6 -> 1-5
                {
                    LevelSetting.nowLevel = 4;
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(9);
                }
            }

            if(b_NPC == true && LevelSetting.talkWithNpc == false)
            {
                b_NPC = false;
                LevelSetting.talkWithNpc = true;
            }
            
            if (b_BarDoor == true) //酒吧
            {
                b_BarDoor = false;
                SceneManager.LoadScene(6);
                LevelSetting.nowLevel = 1;
            }
            if (b_ElevatorDoor == true)//電梯傳送點
            {
                b_ElevatorDoor = false;
                if (LevelSetting.flag_11 == false && LevelSetting.flag_10 == true && LevelSetting.nowLevel == -1)
                {
                    LevelSetting.talkWithElevator = true;
                }
                else if (LevelSetting.nowLevel == 2)//1-3 -> 1-4
                {
                    m_Player.transform.position = new Vector3(10.3f, 0, 0);
                    SceneManager.LoadScene(8);
                    LevelSetting.nowLevel = 3;
                }
                else if (LevelSetting.nowLevel == 3)//1-4 -> 1-3
                {
                    m_Player.transform.position = new Vector3(12.1f, 0, 0);
                    SceneManager.LoadScene(7);
                    LevelSetting.nowLevel = 2;
                }
                else if (LevelSetting.c_1_8 == true && LevelSetting.nowLevel == -1)//1-2 -> 1-8
                {
                    m_Player.transform.position = new Vector3(12.1f, 0, 0);
                    SceneManager.LoadScene(12);
                    LevelSetting.nowLevel = 7;
                }
                else if (LevelSetting.c_1_8 == true && LevelSetting.nowLevel == 7)//1-8 -> 1-2
                {
                    m_Player.transform.position = new Vector3(12.1f, 0, 0);
                    SceneManager.LoadScene(4);
                    LevelSetting.nowLevel = -1;
                }
                else if (LevelSetting.nowLevel == 5)//1-6 -> 1-9地牢
                {
                    m_Player.transform.position = new Vector3(-5, -1.5f, 0);
                    SceneManager.LoadScene(13);
                    LevelSetting.nowLevel = 8;
                }
                else if (LevelSetting.nowLevel == 8)//1-9地牢 -> 1-6
                {
                    m_Player.transform.position = new Vector3(11.4f, -1.5f, 0);
                    SceneManager.LoadScene(10);
                    LevelSetting.nowLevel = 5;
                }
            }
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //受傷
        if (invincibleTimeCount <= 0 )
        {
            PlayerHealth.paralysTime = 0;
            //炸彈小混混
            if (collision.gameObject.tag == "Hurt2")
            {
                a_FightPlayer.SetBool("Hurt", true);
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 5;

                if (collision.gameObject.transform.position.x >= transform.position.x)
                    rb.velocity = new Vector3(-2.082f, 0, 0);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(2.082f, 0, 0);
            }
            //狂戰士
            if (collision.gameObject.tag == "Hurt3")
            {
                a_FightPlayer.SetBool("Hurt", true);
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 10;

                if (collision.gameObject.transform.position.x > transform.position.x)
                    rb.velocity = new Vector3(-2.082f, 0, 0);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(2.082f, 0, 0);
            }

            //唐納德系列
            //跳躍重擊
            if (collision.gameObject.tag == "JumpAttack")
            {
                a_FightPlayer.SetBool("Hurt", true);
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 7;

                if (collision.gameObject.transform.position.x >= transform.position.x)
                    playerController.transform.position = new Vector3(transform.position.x - speed * 2, transform.position.y, transform.position.z);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    playerController.transform.position = new Vector3(transform.position.x + speed * 2, transform.position.y, transform.position.z);
            }
            if (collision.gameObject.tag == "DonaldBombAttack")
            {
                a_FightPlayer.SetBool("Hurt", true);
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 6;

                if (collision.gameObject.transform.position.x >= transform.position.x)
                    playerController.transform.position = new Vector3(transform.position.x - speed * 2, transform.position.y, transform.position.z);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    playerController.transform.position = new Vector3(transform.position.x + speed * 2, transform.position.y, transform.position.z);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NPC")
        {
            b_NPC = true;
            m_InteractiveButtom.SetActive(true);
        }
        if (collision.gameObject.tag == "BarDoor" && LevelSetting.c_1_1_2 == true)//酒吧
        {
            if (LevelSetting.flag_8 == false ||
                LevelSetting.flag_10 == false)
            {
                b_BarDoor = true;
                m_InteractiveButtom.SetActive(true);
            }
        }
        if (collision.gameObject.tag == "ElevatorDoor")//電梯傳送點
        {
            if (enemySave == null)
            {
                if (LevelSetting.flag_11 == false && LevelSetting.flag_10 == true && LevelSetting.nowLevel == -1 ||
                LevelSetting.nowLevel == 2 ||
                LevelSetting.nowLevel == 3 ||
                LevelSetting.c_1_8 == true && LevelSetting.nowLevel == -1 ||
                LevelSetting.c_1_8 == true && LevelSetting.nowLevel == 7 ||
                LevelSetting.nowLevel == 5 ||
                LevelSetting.nowLevel == 8)
                {
                    m_InteractiveButtom.SetActive(true);
                    b_ElevatorDoor = true;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NPC")
        {
            b_NPC = false;
            m_InteractiveButtom.SetActive(false);
        }
        if (collision.gameObject.tag == "BarDoor")
        {
            b_BarDoor = false;
            m_InteractiveButtom.SetActive(false);
        }
        if (collision.gameObject.tag == "ElevatorDoor")
        {
            b_ElevatorDoor = false;
            m_InteractiveButtom.SetActive(false);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "SceneWall_U")
        {
            cantUp = true;
        }
        if (collision.gameObject.tag == "SceneWall_B")
        {
            cantDown = true;
        }
        if (collision.gameObject.tag == "SceneWall_L")
        {
            cantLeft = true;
        }
        if (collision.gameObject.tag == "SceneWall_R")
        {
            cantRight = true;
        }
        if (collision.gameObject.tag == "Finish")//右傳送點
        {
            cantRight = true;
            if (enemySave == null)
            {
                if (LevelSetting.nowLevel == -2 && LevelSetting.flag_10 == true ||
                    LevelSetting.nowLevel == -4 ||
                    LevelSetting.nowLevel == 4 ||
                    LevelSetting.nowLevel == 5 ||
                    LevelSetting.nowLevel == 6 ||
                    LevelSetting.nowLevel == 7 ||
                    LevelSetting.nowLevel == 8)
                {
                    m_InteractiveButtom.SetActive(true);
                    b_FinishPoint = true;
                }
            }
        }
        if (collision.gameObject.tag == "Back")//左傳送點
        {
            cantLeft = true;
            if (enemySave == null)
            {
                if (LevelSetting.nowLevel == 7 ||
                    LevelSetting.nowLevel == 6 ||
                    LevelSetting.nowLevel == 5 ||
                    LevelSetting.nowLevel == 4 ||
                    LevelSetting.nowLevel == 3 ||
                    LevelSetting.nowLevel == -2 ||
                    LevelSetting.nowLevel == -1)
                {
                    m_InteractiveButtom.SetActive(true);
                    b_BackPoint = true;
                }
            }
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "SceneWall_U")
        {
            cantUp = false;
        }
        if (collision.gameObject.tag == "SceneWall_B")
        {
            cantDown = false;
        }
        if (collision.gameObject.tag == "SceneWall_L" || collision.gameObject.tag == "Back")
        {
            cantLeft = false;
        }
        if (collision.gameObject.tag == "SceneWall_R" || collision.gameObject.tag == "Finish")
        {
            cantRight = false;
        }
        if (collision.gameObject.tag == "Finish")
        {
            b_FinishPoint = false;
            cantRight = false;
            m_InteractiveButtom.SetActive(false);
        }
        if (collision.gameObject.tag == "Back")
        {
            b_BackPoint = false;
            cantLeft = false;
            m_InteractiveButtom.SetActive(false);
        }
    }
}
