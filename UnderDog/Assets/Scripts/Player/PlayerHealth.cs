using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    GameObject enemySave;

    public Animator a_Player;
    public Animator a_FightPlayer;
    //public Animator a_FightPlayer;
    Rigidbody2D rb;

    [Header("主角控制器")]
    public GameObject playerController;
    [Header("主角外觀")]
    public GameObject playerSkin;
    [Header("戰鬥主角外觀")]
    public GameObject fightPlayerSkin;
    //被衝撞、炸彈炸到在PlayerCtrl
    [Header("被攻擊無敵時間")]
    public float attackedInvincibleTimeSet = 0.25f;
    [Header("被攻擊硬直時間")]
    public float s_AttackedTimeSet = 0.25f;
    [Header("移動速度")]
    public float speed = 0.75f;

    [Header("(要和唐納德<抱頭摔衝抱時間>一樣)")]
    public float hugHeadInAnimation = 1;

    //特殊狀態時間
    [Header("中毒持續時間")]
    public int poisoningTimeSet = 3;
    public static float poisoningTime;
    float poisoningTimeCount;

    [Header("麻痺持續時間")]
    public int paralysisTimeSet = 1;
    public static float paralysTime;
    float paralysisTimeCount;

    [Header("麻痺")]
    public GameObject m_Paralysis;
    [Header("中毒")]
    public GameObject m_Poisoning;

    static public float inAnimationCount;//主角被抱動畫消失


    // Start is called before the first frame update
    void Start()
    {
        rb = playerController.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = playerController.transform.position;
        PlayerSkin();
        if (UsageCase.isTalking == false)
        {
            Debuff();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //受傷
        if (PlayerCtrl.invincibleTimeCount < 0 && LevelSetting.b_PlayerIsDead == false)
        {
            paralysTime = 0;
            //近戰小混混
            if (collision.gameObject.tag == "Hurt")
            {
                a_FightPlayer.SetBool("Hurt", true);
                PlayerCtrl.invincibleTimeCount = attackedInvincibleTimeSet;
                PlayerCtrl.starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 2;

                if (collision.gameObject.transform.position.x > transform.position.x)
                    rb.velocity = new Vector3(-2.082f, 0, 0);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(2.082f, 0, 0);

                poisoningTime = poisoningTimeSet;
            }
            //遠程小混混
            if (collision.gameObject.tag == "Hurt1")
            {
                a_FightPlayer.SetBool("Hurt", true);
                LevelSetting.deadTimeCount -= 5;

                rb.velocity = new Vector3(0, 0, 0);
                paralysTime = paralysisTimeSet;
            }
            //蓄力一拳
            if (collision.tag == "OnePunch")
            {
                a_FightPlayer.SetBool("Hurt", true);
                PlayerCtrl.invincibleTimeCount = attackedInvincibleTimeSet;
                PlayerCtrl.starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 10;

                if (collision.gameObject.transform.position.x > transform.position.x)
                    rb.velocity = new Vector3(-2.082f, 0, 0);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(2.082f, 0, 0);
            }
        }
    }


    //玩家外觀顯示
    void PlayerSkin()
    {
        //主角變身
        enemySave = GameObject.FindGameObjectWithTag("Enemy");

        //動畫消失
        if (inAnimationCount > 0 && enemySave != null)
        {
            inAnimationCount -= Time.deltaTime;
            fightPlayerSkin.SetActive(false);
        }
        else if (inAnimationCount <= 0 && enemySave != null ||
           LevelSetting.nowDate == 1 && LevelSetting.flag_3 == true)
        {
            playerSkin.SetActive(false);
            fightPlayerSkin.SetActive(true);
        }
        else
        {
            paralysTime = 0;
            poisoningTime = 0;
            playerSkin.SetActive(true);
            fightPlayerSkin.SetActive(false);
        }

        if (LevelSetting.b_PlayerIsDead == true || 
           LevelSetting.nowDate == 1 && LevelSetting.flag_3 == true)
        {
            paralysTime = 0;
            poisoningTime = 0;
            a_FightPlayer.SetBool("Dead", true);

            a_FightPlayer.SetBool("Walk", false);
            a_FightPlayer.SetBool("Run", false);
            a_FightPlayer.SetBool("Attack", false);
            a_FightPlayer.SetBool("Attack_1", false);
            a_FightPlayer.SetBool("Flash", false);
            a_FightPlayer.SetBool("Hurt", false);
        }
        if (LevelSetting.b_PlayerIsDead == false && LevelSetting.nowDate != 1)
        {
            a_FightPlayer.SetBool("Dead", false);
        }

        if (UsageCase.isTalking == true)
        {
            paralysTime = 0;
            poisoningTime = 0;
            rb.velocity = new Vector3(0, 0, 0);
        }
    }

    //異常狀態
    void Debuff()
    {
        //中毒
        if (poisoningTime > 0)
        {
            m_Poisoning.SetActive(true);

            poisoningTimeCount += Time.deltaTime;
            if (poisoningTimeCount >= 1)
            {
                poisoningTime -= 1;
                LevelSetting.deadTimeCount -= 1;
                poisoningTimeCount = 0;
            }
        }
        if (poisoningTime <= 0)
        {
            m_Poisoning.SetActive(false);
        }
        //麻痺
        if (paralysTime > 0)
        {
            m_Paralysis.SetActive(true);

            paralysisTimeCount += Time.deltaTime;
            if (paralysisTimeCount >= 1)
            {
                paralysTime -= 1;
                paralysisTimeCount = 0;
            }
        }
        else
        {
            m_Paralysis.SetActive(false);
        }
    }
}
