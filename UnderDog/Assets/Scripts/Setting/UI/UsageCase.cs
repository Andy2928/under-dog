﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RemptyTool.ES_MessageSystem;

[RequireComponent(typeof(ES_MessageSystem))]
public class UsageCase : MonoBehaviour
{
    [Header("當前文本")]
    public TextAsset textAsset;

    public static bool isSkip = false;

    public static bool isTalking = false;

    public float speed;

    private ES_MessageSystem msgSys;

    public UnityEngine.UI.Image m_Text;
    public UnityEngine.UI.Image m_Text_1;
    public UnityEngine.UI.Image m_Text_2;

    public UnityEngine.UI.Text uiText;
    public UnityEngine.UI.Text uiText_1;
    public UnityEngine.UI.Text uiText_2;
    public GameObject skipBtn;
    int UI_Count = 0;

    private List<string> textList = new List<string>();
    private int textIndex = 0;

    void Start()
    {
        msgSys = this.GetComponent<ES_MessageSystem>();
    }

    public void ReadTextDataFromAsset(TextAsset _textAsset)
    {
        //對話框位置
        m_Text.transform.localPosition = Vector3.zero;
        m_Text_1.transform.localPosition = Vector3.zero;
        m_Text_2.transform.localPosition = Vector3.zero;
        //對話框出現
        m_Text.gameObject.SetActive(true);
        m_Text_1.gameObject.SetActive(true);
        m_Text_2.gameObject.SetActive(true);
        skipBtn.SetActive(true);
        //對話重置
        textList.Clear();
        textList = new List<string>();
        textIndex = 0;
        var lineTextData = _textAsset.text.Split('\n');
        foreach (string line in lineTextData)
        {
            textList.Add(line);
        }
    }

    void Update()
    {
        plotCtrl();

        //Debug.Log(isSkip);

        if (ES_MessageSystem.writeDone == true)
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
            {
                //Continue the messages, stoping by [w] or [lr] keywords.
                msgSys.Next();
                if (ES_MessageSystem.writeStay == false)
                {
                    UI_Count++;
                }
                ES_MessageSystem.writeDone = false;
            }
        }

        //If the message is complete, stop updating text.
        if (msgSys.IsCompleted == false)
        {
            if (UI_Count == 0)
            {
                uiText.text = msgSys.text;
            }
            if (UI_Count == 1)
            {
                uiText_1.text = msgSys.text;

                m_Text.transform.localPosition = Vector3.Lerp(m_Text.transform.localPosition, new Vector3(0, 330, 0), 0.5f);
            }
            if (UI_Count == 2)
            {
                uiText_2.text = msgSys.text;

                m_Text_2.transform.localPosition = Vector3.zero;
                m_Text_1.transform.localPosition = Vector3.Lerp(m_Text_1.transform.localPosition, new Vector3(0, 330, 0), 0.5f);
                m_Text.transform.localPosition = Vector3.Lerp(m_Text.transform.localPosition, new Vector3(0, 660, 0), 0.5f);
            }
            if (UI_Count == 3)
            {
                uiText.text = msgSys.text;

                m_Text_2.transform.localPosition = Vector3.Lerp(m_Text_2.transform.localPosition, new Vector3(0, 330, 0), 0.5f);
                m_Text_1.transform.localPosition = Vector3.Lerp(m_Text_1.transform.localPosition, new Vector3(0, 660, 0), 0.5f);
                m_Text.transform.localPosition = Vector3.zero;
            }
            if (UI_Count == 4)
            {
                uiText_1.text = msgSys.text;

                m_Text_2.transform.localPosition = Vector3.Lerp(m_Text_2.transform.localPosition, new Vector3(0, 660, 0), 0.5f);
                m_Text_1.transform.localPosition = Vector3.zero;
                m_Text.transform.localPosition = Vector3.Lerp(m_Text.transform.localPosition, new Vector3(0, 330, 0), 0.5f);
            }
            if (UI_Count == 5)
            {
                UI_Count = 2;
            }
        }

        //Auto update from textList.
        if (msgSys.IsCompleted == true && textIndex < textList.Count)
        {
            isTalking = true;
            msgSys.SetText(textList[textIndex]);
            textIndex++;
        }


        if (textIndex == textList.Count)
        {
            isTalking = false;
            m_Text.gameObject.SetActive(false);
            m_Text_1.gameObject.SetActive(false);
            m_Text_2.gameObject.SetActive(false);
            skipBtn.SetActive(false);
        }
    }

    public void SkipTalk()
    {
        textIndex = textList.Count;
        isSkip = true;
    }

    //劇情控制
    void plotCtrl ()
    {
        if (LevelSetting.b_sleepCount == false && LevelSetting.isTransitions == true)
        {
            if (LevelSetting.nowDate == 1 && isTalking == false && LevelSetting.flag == false)
            {
                textAsset = Resources.Load("1024-1") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                //正在說話
                isTalking = true;
                LevelSetting.flag = true;
            }
            if (LevelSetting.nowDate == 1 && isTalking == false && LevelSetting.flag_1 == false)
            {
                textAsset = Resources.Load("1024-2") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_1 = true;
            }
            if (LevelSetting.nowDate == 1 && isTalking == false && LevelSetting.flag_2 == false && LevelSetting.nowLevel == -3)
            {
                textAsset = Resources.Load("1024-3_D") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_2 = true;
            }
            if (LevelSetting.nowDate == 1 && isTalking == false && LevelSetting.flag_3 == false && LevelSetting.nowLevel == -3 && LevelSetting.c_1_2 == true)
            {
                textAsset = Resources.Load("1024-4_D") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_3 = true;
            }
            if (LevelSetting.nowDate == 2 && isTalking == false && LevelSetting.flag_4 == false)
            {
                textAsset = Resources.Load("1025-1") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_4 = true;
            }
            if (LevelSetting.nowDate == 3 && isTalking == false && LevelSetting.flag_5 == false)
            {
                textAsset = Resources.Load("1026-1") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_5 = true;
            }
            if (LevelSetting.nowDate == 3 && isTalking == false && LevelSetting.flag_6 == false && LevelSetting.nowLevel == -2 && LevelSetting.talkWithNpc == true)
            {
                textAsset = Resources.Load("1026-2") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_6 = true;
            }
            if (LevelSetting.nowDate == 3 && isTalking == false && LevelSetting.flag_7 == false && LevelSetting.nowLevel == -2 && LevelSetting.c_1_1_2 == true)
            {
                textAsset = Resources.Load("1026-3") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_7 = true;
            }
            if (LevelSetting.nowDate == 3 && isTalking == false && LevelSetting.flag_8 == false && LevelSetting.nowLevel == 1)
            {
                textAsset = Resources.Load("1026-4") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_8 = true;
            }
            if (LevelSetting.nowDate == 4 && isTalking == false && LevelSetting.flag_9 == false && LevelSetting.nowLevel == 0)
            {
                textAsset = Resources.Load("1027-1") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_9 = true;
            }
            if (LevelSetting.nowDate == 4 && isTalking == false && LevelSetting.flag_10 == false && LevelSetting.nowLevel == 1)
            {
                textAsset = Resources.Load("1027-2") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_10 = true;
            }
            if (LevelSetting.nowDate == 4 && isTalking == false && LevelSetting.flag_11 == false && LevelSetting.talkWithElevator == true)
            {
                textAsset = Resources.Load("1027-3") as TextAsset;
                ReadTextDataFromAsset(textAsset);
                UI_Count = 0;

                isTalking = true;
                LevelSetting.flag_11 = true;
                LevelSetting.talkWithElevator = false;
            }

        }
    }
}
