using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class LevelSetting : MonoBehaviour
{
    [Header("死亡CG")]
    [SerializeField] GameObject m_FailCG;
    [SerializeField] GameObject m_WinCG;
    float deadCGCount;

    [Header("地圖相關")]
    public GameObject m_Map;
    public Button m_1_1_2;
    public GameObject img_1_1_2;

    [Header("按鈕")]
    public Button itemBtn;
    public Button newsBtn;
    public Button powerOffBtn;
    public Button saveBtn;
    public Button sleepBtn;
    public Button dateBtn;
    public Button mapBtn;

    [Header("存檔相關")]
    public GameObject m_SaveUI;
    public Button loadBtn;
    public Button deleteBtn;
    public Button loadBtn_1;
    public Button deleteBtn_1;
    public Button loadBtn_2;
    public Button deleteBtn_2;

    public static bool b_PlayerIsDead;
    [Header("死亡動畫時間")]
    public float deadAnimationTime = 3;
    public float deadAnimationTimeCount = 0;

    public static GameObject m_Player;

    public Button rightPad;
    public GameObject pad;
    bool padIsOpen = false;

    public Text t_Date;


    int dateMonth, dateDay;

    //偵測敵人是否清空
    public static GameObject enemySave;
    bool enemyIsClear = true;//從false到true會觸發，所以預設為true
    public GameObject stageClearCG;

    [Header("回家鈕")]
    public Button backToHomeBtn;

    //過場
    float timeCount;
    [Header("過場時間")]
    public float timeSet = 3;
    public static bool isTransitions;
    [Header("過場圖(在Main Camera)")]
    public GameObject img_Transitions;
    public Image sleepUI;
    public static bool b_sleepCount;
    float sleepCount;

    //終端
    //public GameObject m_Camera;

    //時間控制
    [Header("時間最大值")]
    public float LevelMaxTime = 60;
    [Header("連續戰鬥時間增加")]
    public float LevelAddTime = 5;
    public GameObject m_DeadTime;
    public static bool timeIsSet;//進入戰鬥場景是否已經加秒
    public Text t_DeadTime;

    //需要存取的項目
    //和NPC對話部分(n)
    public static bool talkWithNpc;
    public static bool talkWithElevator;

    //血量(t)
    public static float deadTimeCount = 0;

    //當前關卡(l)
    public static float nowLevel = 0;

    //當前日期(d)
    public static int nowDate = 0;

    //flag類(falg = 劇情，c_1_1 = 場景清理)
    public static bool flag = false;//第一天第一段
    public static bool flag_1 = false;//第一天第二段
    public static bool flag_2 = false;//第一天1-2第一段
    public static bool flag_3 = false;//第一天1-2第二段
    public static bool flag_4 = false;//第二天客廳對話
    public static bool flag_5 = false;//第三天客廳對話
    public static bool flag_6 = false;
    public static bool flag_7 = false;
    public static bool flag_8 = false;
    public static bool flag_9 = false;
    public static bool flag_10 = false;
    public static bool flag_11 = false;

    public static bool c_1_2 = false;
    public static bool c_1_1_2 = false;
    public static bool c_1_3 = false;
    public static bool c_1_4 = false;
    public static bool c_1_5 = false;
    public static bool c_1_6 = false;
    public static bool c_1_8 = false;

    public static bool b_1 = false;

    // Start is called before the first frame update
    void Start()
    {
        timeIsSet = true;
        nowLevel = -5;
        enemyIsClear = true;
        deadTimeCount = LevelMaxTime / 2;
        isTransitions = true;
        timeCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CheatBtn();
        UICtrl();
        if (UsageCase.isTalking == false)
        {
            MapCtrl();
            PlotCtrl();
            DateDuringCtrl();
            //終端控制套用(83~)
            PhoneCtrl();
            //時間控制
            DeadTimeControl();
            //過場
        }
    }

    void CheatBtn()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            deadTimeCount = 9999;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            flag = true;
            flag_1 = true;
            flag_2 = true;
            flag_3 = true;
            flag_4 = true;
            flag_5 = true;
            flag_6 = true;
            flag_7 = true;
            flag_8 = true;
            flag_9 = true;
            flag_10 = true;
            flag_11 = true;
            c_1_2 = true;
            c_1_1_2 = true;
            talkWithNpc = true;

            isTransitions = true;
            UsageCase.isTalking = false;

            backToHomeBtn.interactable = true;
            itemBtn.interactable = false;
            newsBtn.interactable = false;
            sleepBtn.interactable = true;
            dateBtn.interactable = false;
            mapBtn.interactable = true;

            nowDate = 4;

            nowLevel = -1;
            SceneManager.LoadScene(4);

        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            isTransitions = true;
            UsageCase.isTalking = false;

            nowLevel = 9;
            SceneManager.LoadScene(14);
        }
    }
    //劇情控制
    void PlotCtrl()
    {
        if (nowDate == 1)
        {
            itemBtn.interactable = false;
            newsBtn.interactable = false;
            sleepBtn.interactable = false;
            dateBtn.interactable = false;
            mapBtn.interactable = false;
            if (flag_3 == true)
            {
                pad.transform.localPosition = new Vector3(1450, 0, 0);
                m_Player.transform.position = new Vector3(-2000, 0, 0);//玩家客廳位置

                nowLevel = 0;
                Sleep();

                SceneManager.LoadScene(5);
            }
        }
        if (nowDate == 2)
        {
            sleepBtn.interactable = true;
        }
        if (nowDate == 3)
        {
            sleepBtn.interactable = false;
            mapBtn.interactable = true;
            if (flag_8 == true)
            {
                pad.transform.localPosition = new Vector3(1450, 0, 0);
                m_Player.transform.position = new Vector3(-2000, 0, 0);//玩家客廳位置

                nowLevel = 0;
                Sleep();

                SceneManager.LoadScene(5);
            }
        }
        if (nowDate == 4)
        {
            if (flag_10 == true && nowLevel == 1)
            {
                isTransitions = false;

                nowLevel = -2;

                SceneManager.LoadScene(3);
            }
            if (flag_11 == true)
            {
                if (enemySave == null)
                {
                    backToHomeBtn.interactable = true;
                }
                itemBtn.interactable = false;
                newsBtn.interactable = false;
                sleepBtn.interactable = true;
                dateBtn.interactable = false;
                mapBtn.interactable = true;
            }
        }

        if (nowDate == 9 && b_1 == false)//50%死亡圖
        {
            m_FailCG.SetActive(true);
            deadCGCount += Time.deltaTime;
            if (deadCGCount >= 5)
            {
                Initialization();
                deadCGCount = 0;
                SceneManager.LoadScene(0);
                m_FailCG.SetActive(false);

                Destroy(m_Player);
                Destroy(gameObject);
            }
        }
        if (b_1 == true)
        {
            deadCGCount += Time.deltaTime;
            if (deadCGCount >= 2)
            {
                m_WinCG.SetActive(true);
            }
        }
    }


    ////終端控制
    void PhoneCtrl()
    {
        bool mouseOnRightPad = RectTransformUtility.RectangleContainsScreenPoint(rightPad.image.rectTransform, Input.mousePosition);

        if (padIsOpen == false)
        {
            if (mouseOnRightPad == true)
            {
                pad.transform.localPosition = Vector3.Lerp(pad.transform.localPosition, new Vector3(1370, 0, 0), 0.5f);
            }
            if (mouseOnRightPad == false)
            {
                pad.transform.localPosition = Vector3.Lerp(pad.transform.localPosition, new Vector3(1450, 0, 0), 0.5f);
            }
        }
        if (padIsOpen == true)
        {
            pad.transform.localPosition = Vector3.Lerp(pad.transform.localPosition, new Vector3(0, 0, 0), 0.5f);
        }

        if (Input.GetKeyDown(KeyCode.Tab) && nowLevel == 0)
        {
            padIsOpen = !padIsOpen;
        }

        if (nowLevel == 0)
        {
            rightPad.gameObject.SetActive(true);
            pad.gameObject.SetActive(true);
        }
        else
        {
            rightPad.gameObject.SetActive(false);
            pad.gameObject.SetActive(false);
        }
    }
    //打開終端
    public void PhoneOnClick()
    {
        padIsOpen = !padIsOpen;
    }
    //點終端以外關掉終端
    public void PhoneScreenClickOut()
    {
        padIsOpen = false;
    }

    ////地圖控制
    void MapCtrl()
    {
        bool mouseOn1_1_2 = RectTransformUtility.RectangleContainsScreenPoint(m_1_1_2.image.rectTransform, Input.mousePosition);
        if (mouseOn1_1_2 == true)
        {
            img_1_1_2.SetActive(true);
        }
        else
        {
            img_1_1_2.SetActive(false);
        }
    }
    //打開地圖
    public void MapOnClick()
    {
        m_Map.SetActive(true);
        padIsOpen = false;
    }
    //點地圖以外關掉地圖
    public void MapOnClickOut()
    {
        m_Map.SetActive(false);
    }

    //打開存檔
    public void SaveOnClick()
    {
        m_SaveUI.SetActive(true);
        padIsOpen = false;
    }
    public void SaveOnClickOut()
    {
        m_SaveUI.SetActive(false);
    }

    //選關
    public void LivingRoomChoose()
    {
        padIsOpen = false;
        pad.transform.localPosition = new Vector3(1450, 0, 0);

        isTransitions = false;

        m_Player.transform.position = new Vector3(-2000, 0, 0);//玩家客廳位置
        nowLevel = 0;
        SceneManager.LoadScene(5);
    }
    public void Level1_1_2()
    {
        PlayerCtrl.cantDown = false;
        PlayerCtrl.cantLeft = false;
        PlayerCtrl.cantRight = false;
        PlayerCtrl.cantUp = false;
        isTransitions = false;

        enemyIsClear = true;

        m_Player.transform.position = new Vector3(-5, -1.5f, 0);
        nowLevel = -2;
        SceneManager.LoadScene(3);
        m_Map.SetActive(false);
    }

    //過場、戰鬥結束UI控制
    void UICtrl()
    {
        if (b_sleepCount == true)
        {
            sleepUI.gameObject.SetActive(true);
            sleepCount += Time.deltaTime;
            if (sleepCount >= 2)
            {
                b_sleepCount = false;
                sleepCount = 0;
            }
        }
        if (b_sleepCount == false)
        {
            sleepUI.gameObject.SetActive(false);
        }

        enemySave = GameObject.FindGameObjectWithTag("Enemy");

        if (nowLevel == 0 || nowLevel == 1 || nowLevel == -5 || flag_11 == false)
        {
            backToHomeBtn.gameObject.SetActive(false);
        }
        if (nowLevel != 0 && nowLevel != 1 && nowLevel != -5 && flag_11 == true)
        {
            backToHomeBtn.gameObject.SetActive(true);
        }

        //是否清過場 & 場景清空CG
        if (enemySave == null)
        {
            if (nowDate > 4)
            {
                backToHomeBtn.interactable = true;
            }
            if (enemyIsClear == false)
            {
                if (nowLevel != 0)
                {
                    stageClearCG.SetActive(true);
                }
                if (nowLevel == -3)
                {
                    c_1_2 = true;
                }
                if (nowLevel == -2)
                {
                    c_1_1_2 = true;
                }
                if (nowLevel == 2)
                {
                    c_1_3 = true;
                }
                if (nowLevel == 3)
                {
                    c_1_4 = true;
                }
                if (nowLevel == 4)
                {
                    c_1_5 = true;
                }
                if (nowLevel == 5)
                {
                    c_1_6 = true;
                }
                if (nowLevel == 7)
                {
                    c_1_8 = true;
                }
                if (nowLevel == 9)
                {
                    b_1 = true;
                }
                enemyIsClear = true;
            }
        }
        else
        {
            backToHomeBtn.interactable = false;
        }
        //讀檔圖示
        if (InitializationSetting.s_0 == true)
        {
            loadBtn.interactable = true;
            deleteBtn.interactable = true;
        }
        else
        {
            loadBtn.interactable = false;
            deleteBtn.interactable = false;
        }
        if (InitializationSetting.s_1 == true)
        {
            loadBtn_1.interactable = true;
            deleteBtn_1.interactable = true;
        }
        else
        {
            loadBtn_1.interactable = false;
            deleteBtn_1.interactable = false;
        }
        if (InitializationSetting.s_2 == true)
        {
            loadBtn_2.interactable = true;
            deleteBtn_2.interactable = true;
        }
        else
        {
            loadBtn_2.interactable = false;
            deleteBtn_2.interactable = false;
        }

        //過場
        if (isTransitions == false)
        {
            img_Transitions.SetActive(true);
            timeCount = timeCount + Time.deltaTime;
            if (timeCount > timeSet)
            {
                img_Transitions.SetActive(false);
                timeCount = 0;
                isTransitions = true;
            }
        }
    }

    //戰鬥時間控制
    void DeadTimeControl()
    {
        if (enemySave != null && isTransitions == true && b_PlayerIsDead == false)
        {
            if (nowLevel == -3 ||
                nowLevel == -2 && c_1_1_2 == false ||
                nowLevel == 2 && c_1_3 == false ||
                nowLevel == 3 && c_1_4 == false ||
                nowLevel == 4 && c_1_5 == false ||
                nowLevel == 5 && c_1_6 == false ||
                nowLevel == 7 && c_1_8 == false ||
                nowLevel == 9 && b_1 == false)
            {
                if (timeIsSet == false)
                {
                    deadTimeCount += LevelAddTime;
                    if (deadTimeCount > LevelMaxTime)
                    {
                        deadTimeCount = LevelMaxTime;
                    }
                    timeIsSet = true;
                }

                m_DeadTime.SetActive(true);
                deadTimeCount = deadTimeCount - Time.deltaTime;
                enemyIsClear = false;
            }
        }
        else
        {
            m_DeadTime.SetActive(false);
        }

        if (deadTimeCount < 0)
        {
            b_PlayerIsDead = true;
            deadAnimationTimeCount += Time.deltaTime;

            if (deadAnimationTimeCount >= deadAnimationTime)
            {
                deadAnimationTimeCount = 0;
                //時間重置
                deadTimeCount = 0;
                timeIsSet = false;//回客廳補時
                //終端狀態設置
                pad.transform.localPosition = new Vector3(1450, 0, 0);
                nowLevel = 0;
                m_Player.transform.position = new Vector3(-2000, -2, 0);
                SceneManager.LoadScene(5);

                b_PlayerIsDead = false;

                Sleep();
            }
        }

        t_DeadTime.text = Mathf.Floor(deadTimeCount).ToString();
    }
    //睡覺
    public void Sleep()
    {
        if (nowDate == 2)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1026");
        }
        else if (nowDate == 3)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1027");
        }
        else if (nowDate == 4)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1028");
        }
        else if (nowDate == 5)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1029");
        }
        else if (nowDate == 6)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1030");
        }
        else if (nowDate == 7)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1031");
        }
        else if (nowDate == 8)
        {
            sleepUI.sprite = Resources.Load<Sprite>("sleep_1101");
        }

        b_sleepCount = true;

        padIsOpen = false;
        pad.transform.localPosition = new Vector3(1450, 0, 0);
        deadTimeCount = LevelMaxTime;

        nowDate += 1;
    }
    //回家
    public void BackToHome()
    {
        timeIsSet = false;
        padIsOpen = false;
        pad.transform.localPosition = new Vector3(1450, 0, 0);

        m_Player.transform.position = new Vector3(-2000, 0, 0);
        nowLevel = 0;
        Sleep();
        SceneManager.LoadScene(5);
    }
    //日期計算
    void DateDuringCtrl()
    {
        if (nowDate <= 8)
        {
            dateMonth = 10 + nowDate / 31;
            dateDay = nowDate - ((dateMonth - 10) * 30) + 23;
        }
        else
        {
            dateMonth = 11 + (nowDate - 8) / 31;
            dateDay = (nowDate - 8) - ((dateMonth - 11) * 30);
        }

        if (nowDate > 360)
        {
            dateMonth = dateMonth - 12;
        }
        if (dateDay >= 32)
        {
            dateMonth += 1;
            dateDay -= 31;
        }
        if (dateDay == 0)
        {
            dateDay = 1;
        }
        if (dateMonth >= 9 && dateDay >= 10)
        {
            t_Date.text = "20XX/" + dateMonth + "/" + dateDay;
        }
        if (dateMonth < 9 && dateDay >= 10)
        {
            t_Date.text = "20XX/0" + dateMonth + "/" + dateDay;
        }
        if (dateMonth < 9 && dateDay < 10)
        {
            t_Date.text = "20XX/0" + dateMonth + "/0" + dateDay;
        }
        if (dateMonth >= 9 && dateDay < 10)
        {
            t_Date.text = "20XX/" + dateMonth + "/0" + dateDay;
        }
    }
    public void StartBtn()
    {
        //切換到場景1
        SceneManager.LoadScene(1);
    }
    public void ExitBtn()
    {
        //離開遊戲
        Application.Quit();
    }
    void Initialization()//初始化
    {
        m_Player.transform.position = new Vector3(13.7f, -0.5f, 0);

        //血量
        deadTimeCount = 30;

        //當前關卡
        nowLevel = 0;

        //當前日期
        nowDate = 0;

        //和NPC對話部分
        talkWithNpc = false;

        //flag類(falg = 劇情，c_1_1 = 場景清理)
        flag = false;//第一天第一段
        flag_1 = false;//第一天第二段
        flag_2 = false;//第一天1-2第一段
        flag_3 = false;//第一天1-2第二段
        flag_4 = false;//第二天客廳對話
        flag_5 = false;//第三天客廳對話
        flag_6 = false;
        flag_7 = false;
        flag_8 = false;
        flag_9 = false;
        flag_10 = false;
        flag_11 = false;

        c_1_2 = false;
        c_1_1_2 = false;
        b_1 = false;
    }






    public class SaveData
    {
        //玩家位置
        public Vector3 playerPosition;

        //血量
        public float deadTimeCount = 0;

        //當前關卡
        public float nowLevel = 0;

        //當前日期
        public int nowDate = 0;
        
        //和NPC對話部分
        public bool talkWithNpc;

        //flag類(falg = 劇情，c_1_1 = 場景清理)
        public bool flag = false;//第一天第一段
        public bool flag_1 = false;//第一天第二段
        public bool flag_2 = false;//第一天1-2第一段
        public bool flag_3 = false;//第一天1-2第二段
        public bool flag_4 = false;//第二天客廳對話
        public bool flag_5 = false;//第三天客廳對話
        public bool flag_6 = false;
        public bool flag_7 = false;
        public bool flag_8 = false;
        public bool flag_9 = false;
        public bool flag_10 = false;
        public bool flag_11 = false;

        public bool c_1_2 = false;
        public bool c_1_1_2 = false;
        public bool b_1 = false;
    }

    SaveData SavingData()
    {
        var saveData = new SaveData();

        saveData.playerPosition = m_Player.transform.position;
        saveData.deadTimeCount = deadTimeCount;
        saveData.nowLevel = nowLevel;
        saveData.nowDate = nowDate;
        saveData.talkWithNpc = talkWithNpc;
        saveData.flag = flag;
        saveData.flag_1 = flag_1;
        saveData.flag_2 = flag_2;
        saveData.flag_3 = flag_3;
        saveData.flag_4 = flag_4;
        saveData.flag_5 = flag_5;
        saveData.flag_6 = flag_6;
        saveData.flag_7 = flag_7;
        saveData.flag_8 = flag_8;
        saveData.flag_9 = flag_9;
        saveData.flag_10 = flag_10;
        saveData.flag_11 = flag_11;
        saveData.c_1_2 = c_1_2;
        saveData.c_1_1_2 = c_1_1_2;
        saveData.b_1 = b_1;

        return saveData;
    }

    void LoadData(SaveData saveData)
    {
        m_Player.transform.position = saveData.playerPosition;
        deadTimeCount = saveData.deadTimeCount;
        nowLevel = saveData.nowLevel;
        nowDate = saveData.nowDate;
        talkWithNpc = saveData.talkWithNpc;
        flag = saveData.flag;
        flag_1 = saveData.flag_1;
        flag_2 = saveData.flag_2;
        flag_3 = saveData.flag_3;
        flag_4 = saveData.flag_4;
        flag_5 = saveData.flag_5;
        flag_6 = saveData.flag_6;
        flag_7 = saveData.flag_7;
        flag_8 = saveData.flag_8;
        flag_9 = saveData.flag_9;
        flag_10 = saveData.flag_10;
        flag_11 = saveData.flag_11;
        c_1_2 = saveData.c_1_2;
        c_1_1_2 = saveData.c_1_1_2;
        b_1 = saveData.b_1;

        if (nowLevel == 0)
        {
            LivingRoomChoose();
        }
        else if (nowLevel == -2)
        {
            Level1_1_2();
        }
    }

    public void Save()
    {
        InitializationSetting.s_0 = true;
        SaveSystem.SaveJson("PlayerData", SavingData());
    }
    public void Load()
    {
        var saveData = SaveSystem.LoadJson<SaveData>("PlayerData");

        LoadData(saveData);
    }

    public void Save_1()
    {
        InitializationSetting.s_1 = true;
        SaveSystem.SaveJson("PlayerData_1", SavingData());
    }
    public void Load_1()
    {
        var saveData = SaveSystem.LoadJson<SaveData>("PlayerData_1");
        LoadData(saveData);
    }

    public void Save_2()
    {
        InitializationSetting.s_2 = true;
        SaveSystem.SaveJson("PlayerData_2", SavingData());
    }
    public void Load_2()
    {
        var saveData = SaveSystem.LoadJson<SaveData>("PlayerData_2");
        LoadData(saveData);
    }
    public void DeleteJson()
    {
        InitializationSetting.s_0 = false;
        SaveSystem.DeleteFile("PlayerData");
    }
    public void DeleteJson_1()
    {
        InitializationSetting.s_1 = false;
        SaveSystem.DeleteFile("PlayerData_1");
    }
    public void DeleteJson_2()
    {
        InitializationSetting.s_2 = false;
        SaveSystem.DeleteFile("PlayerData_2");
    }
}
