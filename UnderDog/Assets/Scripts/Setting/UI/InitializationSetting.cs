using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializationSetting : MonoBehaviour
{
    public float timeCount;

    //是否存檔過
    public static bool s_0;
    public static bool s_1;
    public static bool s_2;


    private void Start()
    {
        Load();
    }
    private void Update()
    {
        timeCount += Time.deltaTime;

        if (timeCount >= 5)
        {
            Save();
            Load();
            timeCount = 0;
        }
    }

    public class SaveData
    {
        //事件bool

        public bool isSave;
        public bool isSave_1;
        public bool isSave_2;
    }

    SaveData SavingData()
    {
        var InitializationData = new SaveData();
        InitializationData.isSave = s_0;
        InitializationData.isSave_1 = s_1;
        InitializationData.isSave_2 = s_2;

        return InitializationData;
    }

    void LoadData(SaveData InitializationData)
    {
        s_0 = InitializationData.isSave;
        s_1 = InitializationData.isSave_1;
        s_2 = InitializationData.isSave_2;
    }
    public void Save()
    {
        SaveSystem.SaveJson("InitializationData", SavingData());
    }

    public void Load()
    {
        var InitializationData = SaveSystem.LoadJson<SaveData>("InitializationData");
        LoadData(InitializationData);
    }
}
