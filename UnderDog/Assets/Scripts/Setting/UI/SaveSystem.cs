using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class SaveSystem
{
    public static void SaveJson(string filename, object data)
    {
        var json = JsonUtility.ToJson(data);
        var path = Path.Combine(Application.persistentDataPath, filename);

        File.WriteAllText(path, json);
    }

    public static T LoadJson<T>(string filename)
    {
        var path = Path.Combine(Application.persistentDataPath, filename);
        var json = File.ReadAllText(path);
        var data = JsonUtility.FromJson<T>(json);

        return data;
    }

    public static void DeleteFile(string filename)
    {
        var path = Path.Combine(Application.persistentDataPath, filename);

        File.Delete(path);
    }

    public static void FindFile(string filename)
    {
        var path = Path.Combine(Application.persistentDataPath, filename);

        Debug.Log(path);
    }
}

