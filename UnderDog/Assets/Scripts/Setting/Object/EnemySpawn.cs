using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public int m_Level;
    public GameObject[] m_Enemy;
    void Update()
    {
        if (m_Level == 112)
        {
            if (LevelSetting.flag_6 == true && LevelSetting.c_1_1_2 == false)
            {
                if (m_Enemy[0] != null && m_Enemy[1] != null && m_Enemy[2] != null && m_Enemy[3] != null)
                {
                    m_Enemy[0].SetActive(true);
                    m_Enemy[1].SetActive(true);
                    m_Enemy[2].SetActive(true);
                    m_Enemy[3].SetActive(true);
                }
            }
        }
        if (m_Level == 13)
        {
            if (LevelSetting.c_1_3 == true)
            {
                gameObject.SetActive(false);
            }
        }
        if (m_Level == 14)
        {
            if (LevelSetting.c_1_4 == true)
            {
                gameObject.SetActive(false);
            }
        }
        if (m_Level == 15)
        {
            if (LevelSetting.c_1_5 == true)
            {
                gameObject.SetActive(false);
            }
        }
        if (m_Level == 16)
        {
            if (LevelSetting.c_1_6 == true)
            {
                gameObject.SetActive(false);
            }
        }
        if (m_Level == 18)
        {
            if (LevelSetting.c_1_8 == true)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
