using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMove : MonoBehaviour
{
    public float speed;
    bool right;
    bool left = true;
    // Update is called once per frame
    void Update()
    {
        if (transform.localPosition.x > 1280)
        {
            left = true;
            right = false;
        }
        if (transform.localPosition.x < -1280)
        {
            left = false;
            right = true;
        }

        if (left == true)
        {
            transform.localPosition -= Vector3.right * speed * Time.deltaTime;
        }
        if (right == true)
        {
            transform.localPosition += Vector3.right * speed * Time.deltaTime;
        }
    }
}
