using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxControl : MonoBehaviour
{
    public int heal = 3;
    public GameObject heal3;
    public GameObject heal2;
    public GameObject heal1;
    public GameObject heal0;


    private void Update()
    {
        if (heal == 2)
        {
            heal3.SetActive(false);
            heal2.SetActive(true);
        }
        else if (heal == 1)
        {
            heal2.SetActive(false);
            heal1.SetActive(true);
        }
        else if (heal == 0)
        {
            heal1.SetActive(false);
            heal0.SetActive(true);
                
            Destroy(gameObject);

            PlayerCtrl.cantDown = false;
            PlayerCtrl.cantLeft = false;
            PlayerCtrl.cantRight = false;
            PlayerCtrl.cantUp = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Attack")
        {
            heal = heal - 1;
        }
    }
}
